import React from "react";
import { ICourierFiled } from "./app.interfaces";
import "./index.css";
import { SubmitHandler, useForm } from "react-hook-form";

export default function App() {
  const {
    register,
    formState: { errors, isValid },
    handleSubmit,
    reset,
    watch,
  } = useForm<ICourierFiled>({
    mode: "onBlur",
  });

  const onSubmit: SubmitHandler<ICourierFiled> = (data) => {
    alert(JSON.stringify(data));
    reset();
    fetch("https://ya.ru/");
  };

  return (
    <div className="App">
      <h1>Форма регистрации курьера</h1>
      <form onSubmit={handleSubmit(onSubmit)}>
        <input
          {...register("firstName", {
            required: "Поле обязтельно к заполнению!",
            minLength: {
              value: 2,
              message: "Минимум 2 символов!",
            },
          })}
          type="text"
          placeholder="Введите ваше имя"
        />
        <div style={{ height: 40, color: "red" }}>
          {errors?.firstName && <p>{errors?.firstName?.message || "Error!"}</p>}
        </div>
        <input
          {...register("lastName", {
            required: "Поле обязтельно к заполнению!",
            minLength: {
              value: 3,
              message: "Минимум 3 символов!",
            },
          })}
          type="text"
          placeholder="Введите вашу фамилию"
        />
        <div style={{ height: 40, color: "red" }}>
          {errors?.lastName && <p>{errors?.lastName?.message || "Error!"}</p>}
        </div>
        <input
          {...register("email", {
            required: "Поле обязтельно к заполнению!",
            pattern: {
              message: "Введите правильный email",
              value: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/,
            },
          })}
          type="text"
          placeholder="Введите ваш email"
        />
        <div style={{ height: 40, color: "red" }}>
          {errors?.email && <p>{errors?.email?.message || "Error!"}</p>}
        </div>
        <input
          {...register("password", {
            required: "Поле обязтельно к заполнению!",
            pattern: {
              message:
                "Minimum eight characters, at least one letter, one number and one special character:",
              value:
                /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
            },
          })}
          type="text"
          placeholder="Введите ваш пароль"
        />
        <div style={{ height: 40, color: "red" }}>
          {errors?.password && <p>{errors?.password?.message || "Error!"}</p>}
        </div>
        <input
          {...register("confirmPassword", {
            required: "Поле обязтельно к заполнению!",
            validate: (val: string) => {
              if (watch("password") !== val) {
                return "Пароли не совпадают!";
              }
            },
            pattern: {
              message:
                "Минимум 5 символов, хотя бы с одной буквой, цифрой и специальным знаком!",
              value:
                /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{5,}$/,
            },
          })}
          type="text"
          placeholder="Повторите ваш пароль"
        />
        <div style={{ height: 40, color: "red" }}>
          {errors?.confirmPassword && (
            <p>{errors?.confirmPassword?.message || "Error!"}</p>
          )}
        </div>
        <input type="submit" disabled={!isValid} />
      </form>
    </div>
  );
}
